const express = require('express')
const route = express.Router()
const Controlador = require('./controller')

route.post('/login',Controlador.Login)
route.post('/destinatario/insertar',Controlador.InsertarDestinatario)
route.post('/registro/insertar',Controlador.InsertarRegistro)

module.exports = route