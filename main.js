//IMPORTACIONES
require('dotenv').config()
const express = require('express')
const route = require('./route')
const cors = require('cors')
const morgan = require('morgan')

const app = express()

//MIDDLEWARE
app.use(express.urlencoded({extended:"false"}))
app.use(morgan('dev'))
app.use(express.json())
app.use(cors())


//RUTAS
app.get('/',(req,res)=>{
    try {
        console.log('trying')
        res.send('api is alive!!!')
    } catch (error) {
        res.send(error)
    }
    
})

app.use('/apibanco',route)

//FUNCIONES
Main =async () => {
    try {
        const puerto = process.env.PORT || 3000
        app.listen(puerto)
        console.log(`app running on port ${puerto}`)

    } catch (error) {
        console.log(`something is wrong \n${error}`)
    }
}

//ACCIONES
Main()
// app.listen(3003,()=>{
//     console.log('func')
// })