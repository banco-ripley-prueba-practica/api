//INSTANCIADORES
const Controlador = {}
var mysql      = require('mysql');
var pool = mysql.createPool({
    host:'localhost',
    database:'desafio_ripley',
    user:'root',
    password:'ToQNGL391#.'
})

//FUNCIONES
Controlador.Login = async (req,res)=>{
    try {
        const {usuario,contrasenna} = req.body
        pool.query(`CALL spVal_usuario('${usuario}','${contrasenna}');`
            ,(err,resp,field)=>{
                if (err) throw err
                res.send(resp[0])
        })

    } catch (error) {
        res.status(500).send(error)
    }
}

Controlador.InsertarDestinatario = async(req,res)=>{
    try {
        const {nombre,rut,digitoVerificador,correo,fono,banco,cuentaNumero,usuario_id,tipoCuenta} = req.body.destinatario
        pool.query(`CALL spIns_destinatario('${nombre}','${rut}','${digitoVerificador}','${correo}','${fono}','${banco}','${cuentaNumero}',${usuario_id},${tipoCuenta});`
            ,(err,resp,field)=>{
                if (err) throw err
                res.send(resp[0])
        })

    } catch (error) {
        res.status(500).send(error)
    }
}

Controlador.InsertarRegistro = async(req,res)=>{
    try {
        const {monto,destinatario} = req.body
        pool.query(`CALL spIns_registro(${monto},${destinatario});`
            ,(err,resp,field)=>{
                if (err) throw err
                res.send(resp[0])
        })

    } catch (error) {
        res.status(500).send(error)
    }
}

module.exports = Controlador